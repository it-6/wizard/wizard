from tkinter import *
from tkinter import ttk


def show_message():
    label["text"] = entry.get()  # получаем введенный текст


root = Tk()
root.title("Как работают поля ввода с tkinter?")
root.geometry("370x200")  # задаем параметры окна

entry = ttk.Entry()
entry.pack(anchor=NW, padx=6, pady=6) # размещаем поле ввода

btn = ttk.Button(text="Кнопка", command=show_message)
btn.pack(anchor=NW, padx=6, pady=6) # размещаем кнопку

label = ttk.Label()
label.pack(anchor=NW, padx=6, pady=6) # размещаем поле вывода

root.mainloop()
